"""
backuper.py:

Backup-, Verschlüsselungs- und Entschlüsselungs-Tool
"""
import os
import sys
import argparse
from subprocess import call
from datetime import datetime


def backup_configs(config_backup_path):
    """
    Erstellt Listen der installierten Programme, ein Backup der Softwarequellen
    und ein Backup der Keys für die Softwarequellen
    """
    os.mkdir(config_backup_path)
    print("backup/ erstellt")

    packages = "dpkg --get-selections > /home/martin/backup/packages.list"
    call(packages, shell=True)
    print("packages.list gespeichert")

    sources = "sudo cp -R /etc/apt/sources.list* /home/martin/backup/"
    call(sources, shell=True)
    print("sources.list* gespeichert")

    keys = "sudo apt-key exportall > /home/martin/backup/repo.keys"
    call(keys, shell=True)
    print("repo.keys gespeichert")



def backup_files(destination_folder):
    """
    Erstellt ein tar.gz Archiv mit unten aufgeführten Ausnahmen
    """
    backuper_working_dir = os.getcwd()
    os.chdir(destination_folder)

    now = datetime.now()
    timestamp = now.strftime("%Y-%m-%d--%H-%M")
    backup_filename = "backup--{}.tar.gz".format(timestamp)

    tar_backup = ("sudo tar cvpzf {0} "
        # exclude the backup tar itself
        "--exclude={0} "
        # exclude versioning systems
        "--exclude=.git "
        "--exclude=.hg "
        # exclude files not needed in /home/martin
        "--exclude=/home/martin/Downloads "
        "--exclude=/home/martin/scripts "
        "--exclude=/home/martin/VirtualBox\ VMs "
        "--exclude=/home/martin/Videos "
        "--exclude=/home/martin/workspace "
        "--exclude=/home/martin/.adobe "
        "--exclude=/home/martin/.aptitude "
        "--exclude=/home/martin/.audacity-data "
        "--exclude=/home/martin/.audacity_temp "
        "--exclude=/home/martin/.cache "
        "--exclude=/home/martin/.codeintel "
        "--exclude=/home/martin/.dbus "
        "--exclude=/home/martin/.distlib "
        "--exclude=/home/martin/.dropbox "
        "--exclude=/home/martin/.dropbox-dist "
        "--exclude=/home/martin/Dropbox/.dropbox.cache "
        "--exclude=/home/martin/.ecryptfs "
        "--exclude=/home/martin/.furiusisomount "
        "--exclude=/home/martin/.gconf "
        "--exclude=/home/martin/.gimp-2.8 "
        "--exclude=/home/martin/.gnome "
        "--exclude=/home/martin/.gnome2 "
        "--exclude=/home/martin/.gnome2_private "
        "--exclude=/home/martin/.gstreamer-0.10 "
        "--exclude=/home/martin/.gvfs "
        "--exclude=/home/martin/.kde "
        "--exclude=/home/martin/.kdenlive "
        "--exclude=/home/martin/.kivy "
        "--exclude=/home/martin/.local/share/Steam "
        "--exclude=/home/martin/.local/share/Trash "
        "--exclude=/home/martin/.macromedia "
        "--exclude=/home/martin/.miro "
        "--exclude=/home/martin/.nv "
        "--exclude=/home/martin/.openvr "
        "--exclude=/home/martin/.pip "
        "--exclude=/home/martin/.pki "
        "--exclude=/home/martin/.Private "
        "--exclude=/home/martin/.pyrenamer "
        "--exclude=/home/martin/.steam "
        "--exclude=/home/martin/.thumbnails "
        "--exclude=/home/martin/.steam "
        "--exclude=/home/martin/.gksu.lock "
        "--exclude=/home/martin/.steampath "
        "--exclude=/home/martin/.steampid "
        # exclude files not needed in /
        "--exclude=/bin "
        "--exclude=/boot "
        "--exclude=/cdrom "
        "--exclude=/dev "
        "--exclude=/etc "
        "--exclude=/home/downwind "
        "--exclude=/home/.ecryptfs "
        "--exclude=/home/lost+found "
        "--exclude=/lib "
        "--exclude=/lib32 "
        "--exclude=/lib64 "
        "--exclude=/lost+found "
        "--exclude=/media "
        "--exclude=/mnt "
        "--exclude=/opt "
        "--exclude=/proc "
        "--exclude=/root "
        "--exclude=/run "
        "--exclude=/sbin "
        "--exclude=/srv "
        "--exclude=/sys "
        "--exclude=/tmp "
        "--exclude=/usr "
        "--exclude=/var "
        "--exclude=/initrd.img "
        "--exclude=/initrd.img.old "
        "--exclude=/vmlinuz "
        "--exclude=/vmlinuz.old "
        "--exclude-backups "
        "--exclude-caches "
    "/").format(backup_filename)

    call(tar_backup, shell=True)

    os.chdir(backuper_working_dir)



def remove_config_backups(config_backup_path):
    """
    Löscht die temporären Config Backup Dateien und Ordner
    """
    packages = "rm -rf /home/martin/backup/packages.list"
    call(packages, shell=True)
    print("packages.list gelöscht")

    sources = "sudo rm -rf /home/martin/backup/sources.list*"
    call(sources, shell=True)
    print("sources.list* gelöscht")

    keys = "sudo rm -rf /home/martin/backup/repo.keys"
    call(keys, shell=True)
    print("repo.keys gelöscht")

    os.rmdir(config_backup_path)
    print("backup/ gelöscht")


def help():
    """
    Eine kleine Anleitung
    """
    return """
    backup:
    dpkg --get-selections > ~/packages.list
    sudo cp -R /etc/apt/sources.list* /home/martin/backup/
    sudo apt-key exportall > /home/martin/backup/repo.keys

    reinstall:
    sudo apt-key add /home/martin/backup/repo.keys
    sudo cp -R /home/martin/backup/sources.list* /etc/apt/
    sudo apt-get update
    sudo apt-get install dselect
    sudo dpkg --set-selections < /home/martin/backup/packages.list
    sudo apt-get dselect-upgrade -y
    """


def encrypt_backup(file_to_encrypt):
    """
    Verschlüsselt eine Datei mittels gpg
    """
    encrypt = "gpg -c --cipher-algo=twofish {}".format(file_to_encrypt)
    call(encrypt, shell=True)


def decrypt_backup(file_to_decrypt, output_filepath):
    """
    Entschlüsselt eine mit gpg verschlüsselte Datei
    """
    decrypt = "gpg -d --output {1} {0}".format(file_to_decrypt, output_filepath)
    call(decrypt, shell=True)


if __name__ == "__main__":
    # Parse Arguments
    parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter,
                                     description="Save the backup in this directory")
    parser.add_argument("-d",
                        "--destination",
                        metavar="DESTINATION-FOLDERPATH",
                        nargs=1,
                        help=help(),
                        required=False,
                        )

    parser.add_argument("--encrypt",
                        metavar="FILEPATH-TO-ENCRYPT",
                        nargs=1,
                        help="encrypt a file with gpg -c --cipher-algo=twofish",
                        required=False,
                        )

    parser.add_argument("--decrypt",
                        metavar=("FILEPATH-TO-DECRYPT", "FILEPATH-FOR-OUTPUTFILE"),
                        nargs=2,
                        help="decrypt a file with gpg -d",
                        required=False,
                        )

    args = parser.parse_args()

    #  Wenn -d gegeben ist, dann wird ein Backup erstellt
    if args.destination:
        print("BACKUP")
        config_backup_path = "/home/martin/backup"
        backup_configs(config_backup_path)

        destination_folder = args.destination[0]
        if not os.path.exists(destination_folder):
            print("ERROR: Destination folder does not exist. Exiting.")
            sys.exit()

        backup_files(destination_folder)

        remove_config_backups(config_backup_path)

    # Wenn --encrypt gegeben ist, wird eine datei verschlüsselt
    if args.encrypt:
        file_to_encrypt = args.encrypt[0]
        encrypt_backup(file_to_encrypt)

    # Wenn --decrypt gegeben ist, wird eine datei entschlüsselt
    if args.decrypt:
        file_to_decrypt = args.decrypt[0]
        output_filepath = args.decrypt[1]
        decrypt_backup(file_to_decrypt, output_filepath)


    print("*** ALL DONE ***")